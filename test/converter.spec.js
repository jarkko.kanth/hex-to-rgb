

const expect = require('chai').expect;
const converter = require('../src/converter');

describe('Color code converter', () => {
    describe('Hex to RGB conversion', () => {
        it("converts the basic colors", () => {
            const redRgb = converter.HexToRgb("ff0000"); //255,0,0
            const greenRgb = converter.HexToRgb("00ff00"); // 0,255,0
            const blueRgb = converter.HexToRgb("0000ff"); //0,0,255

            expect(redRgb).to.equal("255, 0, 0");
            expect(greenRgb).to.equal("0, 255, 0");
            expect(blueRgb).to.equal("0, 0, 255");
        })
    })
})