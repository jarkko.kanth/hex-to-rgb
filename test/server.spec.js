const expect = require('chai').expect;
const app = require('../src/server');
const request = require('request');
const port = 3000;
describe('Color code converter API', () => {
    before('Start server', (done) => {
        server = app.listen(port, () => {
            console.log(`Server listening on ${port}`)
            done();
        })
    })
    describe('Hex to rgb conversion', () => {
        const url = `http://localhost:${port}/hex-to-rgb?hex=ffffff`;
        it("returns status code 200",(done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                
                done();
            })
        })
        it("returns the color in rgb", (done)=> {
            request(url, (error, response, body) => {
                expect(body).to.equal("255, 255, 255");
                
                done();
            })
        })
    })
    after('Stop server',() => {
        server.close();
    })
})