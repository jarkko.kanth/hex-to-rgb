
module.exports = {
    HexToRgb: (hex) => {
        const redRgb = parseInt(hex.slice(0,2), 16);
        const greenRgb = parseInt(hex.slice(2,4), 16);
        const blueRgb = parseInt(hex.slice(4,6), 16);

        return redRgb.toString() + ", " + greenRgb.toString() + ", " + blueRgb.toString();
    }}