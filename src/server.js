const express = require('express');
const app = express();
const converter = require('./converter');
const port = 3000;

// endpoint localhost:3000/
app.get('/', (req, res) => res.send("Hello"));

app.get('/hex-to-rgb', (req, res)=>{
    const hex = req.query.hex;
    const rgb = converter.HexToRgb(hex);
    res.send(rgb);
})

if(process.env.NODE_ENV === 'test'){
    module.exports = app;
} else{
    app.listen(port, ()=> console.log(`Server listening on localhost:${port}`));
}